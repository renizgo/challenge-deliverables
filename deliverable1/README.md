# Deliverable 1 - Docker

1. Create a clean web Dockerfile
```
FROM node:10.16.0-alpine

WORKDIR /app
COPY . .
RUN npm i
EXPOSE 3000
CMD [ "node", "bin/www" ]
```
2. Create a clean api Dockerfile
```
FROM node:10.16.0-alpine

WORKDIR /app
COPY . .
RUN npm i
EXPOSE 3001
CMD [ "node", "bin/www" ]
```
3. Define a docker-compose.yml in the project root that run the web and api with a
simple docker-compose up
```
version: '3.1'

services:

  db:
    image: postgres
    restart: always
    ports:
    - "5432:5432"
    environment:
      POSTGRES_DB: xerpa
      POSTGRES_PASSWORD: xerpa123
  api:
    image: renizgo/node-api
    restart: always
    links:
      - "db:postgresql"
    depends_on:
      - db
    environment:
      DB: "postgres://postgres:xerpa123@postgresql:5432/xerpa"
      PORT: 3000
    ports:
    - "3000:3000"
  front:
    image: renizgo/node-front
    restart: always
    links:
      - "api:api"
    depends_on:
      - api
    environment:
      PORT: 3000
    ports:
    - "80:3000"
```
4. The docker-compose.yml needs to make the developer life better
```
docker-compose up -d
```
5. Publish the image in a public registry with the command docker-compose push web api
```
MacBook-Pro-de-Renato:deliverable1 renato$ docker images
REPOSITORY           TAG                 IMAGE ID            CREATED             SIZE
renizgo/node-front   latest              1a583930cb4d        4 minutes ago       94.1MB
renizgo/node-api     latest              42653006eba3        5 minutes ago       78.4MB
postgres             latest              6dc0e6af942c        8 days ago          312MB
node                 10.16.0-alpine      9dfa73010b19        5 weeks ago         75.3MB
MacBook-Pro-de-Renato:deliverable1 renato$ docker push renizgo/node-front
The push refers to repository [docker.io/renizgo/node-front]
63f30a08c5f4: Pushed
5e51ef9425c4: Pushed
297f23d43d62: Pushed
109c4a769edd: Mounted from library/node
04db5bd8a39f: Mounted from library/node
eaa2fd82d1ac: Mounted from library/node
f1b5933fe4b5: Mounted from renizgo/jenkins-dind
latest: digest: sha256:540f99932c12225130d14ca0ec62c91deccf911ae3f0af08415c38f291c59b1f size: 1784
MacBook-Pro-de-Renato:deliverable1 renato$ docker push renizgo/node-api
The push refers to repository [docker.io/renizgo/node-api]
9ad8aa030da6: Pushed
637f26799e60: Pushed
297f23d43d62: Mounted from renizgo/node-front
109c4a769edd: Mounted from renizgo/node-front
04db5bd8a39f: Mounted from renizgo/node-front
eaa2fd82d1ac: Mounted from renizgo/node-front
f1b5933fe4b5: Mounted from renizgo/node-front
latest: digest: sha256:86ef53763a8bd3ff61ca5348b537035299ef6cdb92bbee475b353c592e36aa99 size: 1783
```

## Subindo a aplicação

Para subir a aplicação siga os passos abaixo:

Faça o clone:

```
git clone https://renizgo@bitbucket.org/renizgo/challenge-deliverables.git
```

Entre no diretório e execute o Docker Compose:

```
cd challenge-deliverables/deliverable1
docker-compose up -d
```

Resultado esperado

```
MacBook-Pro-de-Renato:deliverable1 renato$ docker-compose up -d
Creating network "deliverable1_default" with the default driver
Creating deliverable1_db_1 ... done
Creating deliverable1_api_1 ... done
Creating deliverable1_front_1 ... done
MacBook-Pro-de-Renato:deliverable1 renato$ docker-compose ps
        Name                      Command               State               Ports
----------------------------------------------------------------------------------------------
deliverable1_api_1     docker-entrypoint.sh node  ...   Up      0.0.0.0:3000->3000/tcp
deliverable1_db_1      docker-entrypoint.sh postgres    Up      0.0.0.0:5432->5432/tcp
deliverable1_front_1   docker-entrypoint.sh node  ...   Up      0.0.0.0:80->3000/tcp, 3001/tcp
```

Acesse via browser

[Link](http://localhost)

![Image01](images/image01.png)
